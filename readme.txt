OneProject - проект для одного хоста: там и front, и back согласно заданию
OneProjectScreens - проект для одного хоста: скрины из IIS и скрин приложения
ReactOneFullHost - проект для одного хоста: целевая папка для сайта IIS

MultiProjectBack - проект для разных хостов: back, CORS
MultiProjectFront - проект для разных хостов: front, dotenv
MultiProjectScreens - проект для разных хостов: скрины из IIS и скрин приложения
ReactMultiHostBack - проект для разных хостов: целевая папка для сайта IIS back
ReactMultiHostFront - проект для разных хостов: целевая папка для сайта IIS front